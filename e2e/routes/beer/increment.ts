import { mockBeer } from "../../../test_utils/mock-dataset";
import supertest from "supertest";
import { connect as connectDatabase, getBeerCollection } from "../../../db";

import { app } from "../../../app";
import { IBeerDocument } from "../../../models/beer";

// These routes are hooked up to the brewery handlers as well
jest.mock("../../../controllers/breweries");

require("dotenv").config({ path: ".env" });

process.env.NODE_ENV = "devlopment";

const mongoDbUri = `mongodb+srv://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@inventory-fcghx.mongodb.net/test?retryWrites=true&w=majority`;

const checkForAndHandleError = (err) => {
  if (err) {
    throw err;
  }
  return;
};

describe("/beer/increment", () => {
  const originalDoc = mockBeer[0];

  let response: supertest.Response;
  let documentUnderTest: IBeerDocument;

  beforeAll((done) => {
    connectDatabase(mongoDbUri, (err) => {
      checkForAndHandleError(err);
      done();
    });
  });

  beforeAll((done) => {
    const collection = getBeerCollection();
    collection.deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      getBeerCollection().insertMany(mockBeer, (err, result) => {
        checkForAndHandleError(err);
        done();
      });
    });
  });

  afterAll((done) => {
    getBeerCollection().deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      done();
    });
  });

  // const makeRequest = (id: string, amount?: number) => {
  //   let requestPromise = supertest(app).patch(`/beer/increment/${id}`);

  //   if (amount) {
  //     requestPromise.send({ amount });
  //   }

  //   return requestPromise.end((res) => {
  //     response = res;
  //   });
  // };

  describe("Successfull request", () => {
    describe("Amount is not provided", () => {
      beforeAll(() => {
        return supertest(app)
          .patch(`/beer/increment${originalDoc._id}`)
          .then((res) => (response = res));
      });

      afterAll((done) => {
        getBeerCollection().updateOne(
          { _id: originalDoc._id },
          { $set: { ...originalDoc } },
          (err, result) => {
            // error out if there was an error or the update failed
            if (err || result.modifiedCount < 1) {
              console.error(result);
              throw err;
            }
            done();
          }
        );
      });

      it("should return 200", () => {
        expect(response.status).toBe(200);
      });

      it("should update currentQuantity by 1 if amount is not specified", (done) => {
        getBeerCollection().findOne({ _id: originalDoc._id }, (err, doc: IBeerDocument) => {
          checkForAndHandleError(err);
          expect(doc.currentQuantity).toBe(originalDoc.currentQuantity + 1);
          done();
        });
      });

      it("should update historicQuantity by 1 if amount is not specified", (done) => {
        getBeerCollection().findOne({ _id: originalDoc._id }, (err, doc: IBeerDocument) => {
          checkForAndHandleError(err);
          expect(doc.historicQuantity).toBe(originalDoc.historicQuantity + 1);
          done();
        });
      });
    });
  });

  describe("Amount is provided", () => {
    const amount = 5;
    beforeAll(() => {
      return supertest(app)
        .patch(`/beer/increment/${originalDoc._id}?amount=${amount}`)
        .then((res) => (response = res));
    });

    afterAll((done) => {
      getBeerCollection().updateOne(
        { _id: originalDoc._id },
        { $set: { ...originalDoc } },
        (err, result) => {
          // error out if there was an error or the update failed
          if (err || result.modifiedCount < 1) {
            console.error(result);
            throw err;
          }
          done();
        }
      );
    });

    it("should return 200", () => {
      expect(response.status).toBe(200);
    });

    it("should update currentQuantity by the specified amount if amount is specified", (done) => {
      getBeerCollection().findOne({ _id: originalDoc._id }, (err, doc: IBeerDocument) => {
        checkForAndHandleError(err);
        expect(doc.currentQuantity).toBe(originalDoc.currentQuantity + amount);
        done();
      });
    });

    it("should update historicQuantity by the specified amount if amount is specified", (done) => {
      getBeerCollection().findOne({ _id: originalDoc._id }, (err, doc: IBeerDocument) => {
        checkForAndHandleError(err);
        expect(doc.historicQuantity).toBe(originalDoc.historicQuantity + amount);
        done();
      });
    });
  });
});

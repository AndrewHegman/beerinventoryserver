import { mockBeer } from "../../../test_utils/mock-dataset";
import supertest from "supertest";
import { connect as connectDatabase, getBeerCollection } from "../../../db";

import { app } from "../../../app";

require("dotenv").config({ path: ".env" });

process.env.NODE_ENV = "devlopment";

const mongoDbUri = `mongodb+srv://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@inventory-fcghx.mongodb.net/test?retryWrites=true&w=majority`;

const checkForAndHandleError = (err) => {
  if (err) {
    throw err;
  }
  return;
};

describe("/beer/current", () => {
  let response: supertest.Response;

  beforeAll((done) => {
    const collection = getBeerCollection();
    collection.deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      getBeerCollection().insertMany(mockBeer, (err, result) => {
        checkForAndHandleError(err);
        done();
      });
    });
  });

  beforeAll((done) => {
    getBeerCollection().insertMany(mockBeer, (err, result) => {
      checkForAndHandleError(err);
      done();
    });
  });

  afterAll((done) => {
    getBeerCollection().deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      done();
    });
  });

  describe("GET", () => {
    beforeEach(() => {
      return supertest(app)
        .get("/beer/current")
        .then((res) => (response = res));
    });
    it("should return 200", () => {
      expect(response.status).toBe(200);
    });
    it("should return only beer with currentQuantity > 0", () => {
      expect(response.body).toEqual(mockBeer.filter((beer) => beer.currentQuantity > 0));
    });
  });
});

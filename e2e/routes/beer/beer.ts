import { mockBeer } from "../../../test_utils/mock-dataset";
import supertest from "supertest";
import { connect as connectDatabase, getBeerCollection } from "../../../db";
import { app } from "../../../app";
import { IBeerDocument } from "../../../models/beer";
import axios, { AxiosResponse, AxiosError } from "axios";

require("dotenv").config({ path: ".env" });

process.env.NODE_ENV = "devlopment";

const mongoDbUri = `mongodb+srv://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@inventory-fcghx.mongodb.net/test?retryWrites=true&w=majority`;

const checkForAndHandleError = (err) => {
  if (err) {
    throw err;
  }
  return;
};

describe("/beer", () => {
  let response: AxiosResponse;

  beforeAll((done) => {
    connectDatabase(mongoDbUri, (err) => {
      checkForAndHandleError(err);
      done();
    });
  });

  beforeAll((done) => {
    const collection = getBeerCollection();
    collection.deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      getBeerCollection().insertMany(mockBeer, (err, result) => {
        checkForAndHandleError(err);
        done();
      });
    });
  });

  afterAll((done) => {
    getBeerCollection().deleteMany({}, (err, result) => {
      checkForAndHandleError(err);
      done();
    });
  });

  describe("GET", () => {
    fdescribe("without an ID", () => {
      beforeAll(() => {
        return axios
          .get("http://localhost:3001/beer")
          .then((res) => {
            response = res;
          })
          .catch((err: AxiosError) => {
            throw err.message;
          });
      });
      it("should return 200", () => {
        expect(response.status).toBe(200);
      });
      it("should return all beer", () => {
        expect(response.data).toEqual(mockBeer);
      });
    });

    describe("with ID(s)", () => {
      describe("Single ID", () => {
        const beerId = mockBeer[0]._id;
        describe("If the ID exists", () => {
          beforeAll(() => {
            return axios.get(`/beer?beerId=${beerId}`).then((res) => (response = res));
          });
          it("should return 200", () => {
            expect(response.status).toBe(200);
          });
          it("should return the correct beer", () => {
            expect(response.data.length).toBe(1);
            expect(response.data).toEqual(mockBeer.filter((beer) => beer._id === beerId));
          });
        });
        describe("If the ID doesn't exist", () => {
          const beerId = "nonExistantBeerId";
          beforeAll(() => {
            return axios.get(`/beer?beerId=${beerId}`).then((res) => (response = res));
          });
          it("should return 200", () => {
            expect(response.status).toBe(200);
          });
          it("should return an empty array", () => {
            expect(response.data.length).toBe(0);
            expect(response.data).toEqual([]);
          });
        });
      });
      describe("Multiple IDs", () => {
        describe("If all of the IDs exist", () => {
          const beerIds = "1,2,3,4";
          beforeAll(() => {
            return axios.get(`/beer?beerId=${beerIds}`).then((res) => (response = res));
          });
          it("should return 200", () => {
            expect(response.status).toBe(200);
          });
          it("should return multiple beers", () => {
            const beerIdsArray: string[] = beerIds.split(",");
            expect(response.data.length).toBe(4);
            expect(response.data).toEqual(mockBeer.filter((beer) => beerIdsArray.indexOf(beer._id) >= 0));
          });
        });

        describe("If some of the IDs exist", () => {
          const beerIds = "1,2,nonExistantBeerId1,nonExistantBeerId2";
          beforeAll(() => {
            return axios.get(`/beer?beerId=${beerIds}`).then((res) => (response = res));
          });
          it("should return 200", () => {
            expect(response.status).toBe(200);
          });
          it("should return multiple beers", () => {
            const beerIdsArray: string[] = beerIds.split(",");
            expect(response.data.length).toBe(2);
            expect(response.data).toEqual(mockBeer.filter((beer) => beerIdsArray.indexOf(beer._id) >= 0));
          });
        });

        describe("If none of the IDs exist", () => {
          const beerIds = "nonExistantBeerId1,nonExistantBeerId2,nonExistantBeerId3,nonExistantBeerId4";
          beforeAll(() => {
            return axios.get(`/beer?beerId=${beerIds}`).then((res) => (response = res));
          });
          it("should return 200", () => {
            expect(response.status).toBe(200);
          });
          it("should return multiple beers", () => {
            const beerIdsArray: string[] = beerIds.split(",");
            expect(response.data.length).toBe(0);
            expect(response.data).toEqual(mockBeer.filter((beer) => beerIdsArray.indexOf(beer._id) >= 0));
          });
        });
      });
    });
  });

  describe("POST", () => {
    describe("Successful request", () => {
      const newBeerDoc: IBeerDocument = {
        _id: "newId",
        name: "newBeerName",
        brewery: "newBreweryName",
        style: "newStyleName",
        container: "can/bottle",
        currentQuantity: 6,
        historicQuantity: 6,
      };

      beforeAll(() => {
        return axios.post("/beer", newBeerDoc).then((res) => (response = res));
      });

      afterAll((done) => {
        getBeerCollection().deleteOne({ _id: newBeerDoc._id }, (err, result) => {
          // error out if there was an error or the delete failed
          if (err || result.deletedCount < 1) {
            throw err;
          }
          done();
        });
      });

      it("should return 200", () => {
        expect(response.status).toBe(200);
      });

      it("should add a new document to the collection", (done) => {
        getBeerCollection().countDocuments({}, (err, result) => {
          if (err) {
            throw err;
          }
          expect(result).toBe(mockBeer.length + 1);
          done();
        });
      });

      it("should add the correct document to the collection", (done) => {
        getBeerCollection().findOne({ _id: newBeerDoc._id }, (err, doc) => {
          checkForAndHandleError(err);
          expect(doc).toEqual(newBeerDoc);
          done();
        });
      });
    });

    describe("Missing body", () => {
      beforeAll(() => {
        return axios.post("/beer").then((res) => (response = res));
      });

      it("should return a 400", () => {
        expect(response.status).toBe(400);
      });

      it("should not add a new document to the collection", () => {
        getBeerCollection()
          .find({})
          .toArray((err, docs) => {
            checkForAndHandleError(err);
            expect(docs.length).toBe(mockBeer.length);
            expect(docs).toEqual(mockBeer);
          });
      });
    });

    describe("Body has invalid/extra fields", () => {
      const invalidNewDocument = {
        _id: "newId",
        name: "newBeerName",
        brewery: "newBreweryName",
        style: "newStyleName",
        container: "can/bottle",
        currentQuantity: 6,
        historicQuantity: 6,
        thisIsAnExtraField: true,
      };
      beforeAll(() => {
        return axios.post("/beer", invalidNewDocument).then((res) => (response = res));
      });

      it("should return a 400", () => {
        expect(response.status).toBe(400);
      });

      it("should not add a new document to the collection", () => {
        getBeerCollection()
          .find({})
          .toArray((err, docs) => {
            checkForAndHandleError(err);
            expect(docs.length).toBe(mockBeer.length);
            expect(docs).toEqual(mockBeer);
          });
      });
    });

    describe("Body is missing a required field", () => {
      const invalidNewDocument = {
        _id: "newId",
        brewery: "newBreweryName",
        style: "newStyleName",
      };
      beforeAll(() => {
        return axios.post("/beer", invalidNewDocument).then((res) => (response = res));
      });

      it("should return a 400", () => {
        expect(response.status).toBe(400);
      });

      it("should not add a new document to the collection", () => {
        getBeerCollection()
          .find({})
          .toArray((err, docs) => {
            checkForAndHandleError(err);
            expect(docs.length).toBe(mockBeer.length);
            expect(docs).toEqual(mockBeer);
          });
      });
    });
  });

  describe("PATCH", () => {
    const originalDoc = mockBeer[0];
    const newBeerDoc: Partial<IBeerDocument> = {
      _id: "newId",
      name: "newBeerName",
      brewery: "newBreweryName",
      style: "newStyleName",
      container: "growler",
      currentQuantity: originalDoc.currentQuantity + 1,
      historicQuantity: originalDoc.historicQuantity + 1,
    };

    describe("Successfull request", () => {
      beforeAll(() => {
        return axios.patch(`/beer/${originalDoc._id}`, newBeerDoc).then((res) => (response = res));
      });

      afterAll((done) => {
        getBeerCollection().updateOne({ _id: originalDoc._id }, { $set: { ...originalDoc } }, (err, result) => {
          // error out if there was an error or the update failed
          if (err || result.modifiedCount < 1) {
            console.error(result);
            throw err;
          }
          done();
        });
      });

      it("should return 200", () => {
        expect(response.status).toBe(200);
      });

      it("should only update the acceptable fields", (done) => {
        getBeerCollection().findOne({ _id: originalDoc._id }, (err, doc) => {
          checkForAndHandleError(err);
          expect(doc).toEqual({
            ...newBeerDoc,
            _id: originalDoc._id,
            currentQuantity: originalDoc.currentQuantity,
            historicQuantity: originalDoc.historicQuantity,
          });
          done();
        });
      });
    });

    describe("Non existant ID", () => {
      beforeAll(() => {
        return axios.patch("/beer/nonExistantBeerId", newBeerDoc).then((res) => (response = res));
      });

      it("should return a 404", () => {
        expect(response.status).toBe(404);
      });

      it("should not change any of the documents", (done) => {
        getBeerCollection()
          .find({})
          .toArray((err, docs) => {
            checkForAndHandleError(err);
            expect(docs).toEqual(mockBeer);
            done();
          });
      });
    });

    describe("Invalid request", () => {
      describe("No body", () => {
        beforeAll(() => {
          return axios.patch(`/beer/${originalDoc._id}`).then((res) => (response = res));
        });

        it("should return a 400", () => {
          expect(response.status).toBe(400);
        });

        it("should not change any of the documents", (done) => {
          getBeerCollection()
            .find({})
            .toArray((err, docs) => {
              checkForAndHandleError(err);
              expect(docs).toEqual(mockBeer);
              done();
            });
        });
      });

      describe("Invalid parameters in body", () => {
        const newBeerDoc = {
          thisPropertyDoesNotExist: true,
        };
        beforeAll(() => {
          return axios.patch(`/beer/${originalDoc._id}`, newBeerDoc).then((res) => (response = res));
        });

        it("should return a 400", () => {
          expect(response.status).toBe(400);
        });

        it("should not change any of the documents", (done) => {
          getBeerCollection()
            .find({})
            .toArray((err, docs) => {
              checkForAndHandleError(err);
              expect(docs).toEqual(mockBeer);
              done();
            });
        });
      });
    });
  });

  describe("DELETE", () => {
    const originalDoc = mockBeer[0];
    beforeAll(() => {
      return axios.delete(`/beer/${originalDoc._id}`).then((res) => (response = res));
    });

    afterAll((done) => {
      getBeerCollection().insertOne(originalDoc, (err, result) => {
        // error out if there was an error or the insert failed
        if (err || result.insertedCount < 1) {
          console.error(result);
          throw err;
        }
        done();
      });
    });

    it("should return 200", () => {
      expect(response.status).toBe(200);
    });

    it("should delete the specified document", (done) => {
      const collection = getBeerCollection();
      collection.find({}).toArray((err, docs) => {
        checkForAndHandleError(err);
        expect(docs.length).toBe(mockBeer.length - 1);
        expect(docs).toEqual(mockBeer.filter((beer) => beer._id !== originalDoc._id));
        done();
      });
    });
  });
});

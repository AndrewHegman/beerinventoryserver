import { MongoLookupType } from "./common";
import { IGeographyDocument } from "../models/geography";

export interface IBreweryDocument {
  _id: string;
  name: string;
  country: string;
  state?: string;
  city?: string;
  count: number;
}

export type ExpandableFields = "country" | "state" | "city";

export const expandableFieldsMap: {
  [key in ExpandableFields]: MongoLookupType<IBreweryDocument, IGeographyDocument>;
} = {
  country: {
    from: "Geography",
    localField: "country",
    foreignField: "_id",
    as: "country",
  },
  state: {
    from: "Geography",
    localField: "state",
    foreignField: "_id",
    as: "state",
  },
  city: {
    from: "Geography",
    localField: "city",
    foreignField: "_id",
    as: "city",
  },
};

export const expandableFields = ["country", "state", "city"];

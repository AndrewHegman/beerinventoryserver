import { MongoLookupType } from "./common";
import { IBreweryDocument } from "./breweries";
import { IStyleDocument } from "./styles";

export type ContainerType = "can/bottle" | "growler" | "crowler" | "none";

export interface IBeerDocument {
  _id: string;
  name: string;
  style: string;
  brewery: string;
  container: ContainerType;
  currentQuantity: number;
  historicQuantity: number;
}

export type ExpandableFields = "brewery" | "style";

export const expandableFieldsMap: {
  [key in ExpandableFields]: MongoLookupType<IBeerDocument, IBreweryDocument | IStyleDocument>;
} = {
  brewery: {
    from: "Breweries",
    localField: "brewery",
    foreignField: "_id",
    as: "brewery",
  },
  style: {
    from: "Styles",
    localField: "style",
    foreignField: "_id",
    as: "style",
  },
};

export const expandableFields = ["brewery", "style"];

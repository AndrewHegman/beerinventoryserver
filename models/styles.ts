import { MongoLookupType } from "./common";

export interface IStyleDocument {
  _id: string;
  name: string;
  baseStyle: string;
  isBaseStyle: boolean;
  count: number;
}

export type ExpandableFields = "baseStyle";

export const expandableFieldsMap: {
  [key in ExpandableFields]: MongoLookupType<IStyleDocument, IStyleDocument>;
} = {
  baseStyle: {
    from: "Styles",
    localField: "baseStyle",
    foreignField: "_id",
    as: "baseStyle",
  },
};

export const expandableFields = ["baseStyles"];

export type Collections = "Beer" | "Breweries" | "Styles" | "Geography";

export interface MongoLookupType<S, D> {
  from: Collections;
  localField: keyof S;
  foreignField: keyof D;
  as: string;
}

export const buildExpandFieldPredicate = (aggregateFragment: any[], expandFieldQuery: string, validFields: string, expandableFieldsMap) => {
  const expandFields = expandFieldQuery
    ?.split(",")
    .filter((field) => validFields.indexOf(field) > -1)
    .map((validField) => ({ $lookup: expandableFieldsMap[validField] }));

  if (expandFields) aggregateFragment.push(...expandFields);
  return aggregateFragment;
};

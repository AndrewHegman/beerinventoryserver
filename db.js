"use strict";
exports.__esModule = true;
var mongodb_1 = require("mongodb");
require("dotenv");
var state = {
    client: null,
    db: null
};
exports.connect = function (url, done) {
    if (state.db)
        return done();
    mongodb_1.MongoClient.connect(url, function (err, db) {
        if (err)
            return done(err);
        state.client = db;
        state.db = db.db(process.env.DATABASE_NAME);
        done();
    });
};
exports.get = function () {
    return state.db;
};
exports.close = function (done) {
    if (state.db) {
        state.client.close(function (err, result) {
            state.db = null;
            done(err);
        });
    }
};
exports.getBeerCollection = function () { return exports.get().collection("Beer"); };
exports.getBreweriesCollection = function () { return exports.get().collection("Breweries"); };
exports.getStylesCollection = function () { return exports.get().collection("Styles"); };

import express from "express";
import * as styles from "../../controllers/styles";

const router = express.Router({ mergeParams: true });

// GET all current styles
router.get("/", styles.current);

// GET quantity of style at given id in inventory (0 if not currently in inventory)
router.get("/:styleId", (req, res, next) => {
  res.send(`Quantity of styles made from ${req.params.styleId} currently in inventory`);
});

export { router };

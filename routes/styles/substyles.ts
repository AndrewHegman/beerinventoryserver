import express from "express";
import * as styles from "../../controllers/styles";
const router = express.Router({ mergeParams: true });

// Fetch all substyles in database of a given style id
router.get("/", styles.getSubstyles);

export { router };

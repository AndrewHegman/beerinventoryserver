import express from "express";
import { router as currentRouter } from "./current";
import { router as substylesRouter } from "./substyles";
import { router as baseStylesRouter } from "./baseStyles";
import * as styles from "../../controllers/styles";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

router.use("/current", currentRouter);
router.use("/basestyles", baseStylesRouter);
router.use("/substyles", substylesRouter);

// Fetch all styles in database
router.get("/", styles.getStyles);

// Add new style
router.post("/", styles.addNew, ok);

// Update style
router.patch("/:styleId", (req, res, next) => {
  res.send(`Update ${req.params.styleId} with data in body`);
});

// Delete style
router.delete("/:styleId", (req, res, next) => {
  res.send(`Delete ${req.params.styleId}`);
});

export { router };

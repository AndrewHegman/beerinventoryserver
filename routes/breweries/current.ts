import express from "express";
import * as breweries from "../../controllers/breweries";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// GET all current beer
router.get("/", breweries.current);

export { router };

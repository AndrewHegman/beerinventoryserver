import express, { Request, Response, NextFunction } from "express";
import * as breweries from "../../controllers/breweries";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// PATCH brewery at given id is incremented by 1 unless `amount` is provided in query
router.patch("/:breweryId", breweries.incrementByBreweryId, ok);

export { router };

import express from "express";
import * as breweries from "../../controllers/breweries";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// PATCH breweries at given id is decremented by 1, unless `amount1` is provided in query
router.patch("/:breweryId", breweries.decrementByBreweryId, ok);

export { router };

import express from "express";
import { router as currentRouter } from "./current";
import { router as incrementRouter } from "./increment";
import { router as decrementRouter } from "./decrement";

import * as breweries from "../../controllers/breweries";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

router.use("/current", currentRouter);
router.use("/increment", incrementRouter);
router.use("/decrement", decrementRouter);

// Fetch all items in database if no beerId(s) are provided
router.get("/", breweries.getBreweries);

router.post("/", breweries.addNew, ok);

// Update existing beer in database (cannot update current quantity or _id)
router.patch("/:breweryId", breweries.updateById, ok);

router.delete("/:breweryId", breweries.deleteById, ok);

export { router };

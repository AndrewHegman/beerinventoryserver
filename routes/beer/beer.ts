import express from "express";
import { router as currentRouter } from "./current";
import { router as incrementRouter } from "./increment";
import { router as decrementRouter } from "./decrement";
import { router as existingRouter } from "./existing";

import * as beer from "../../controllers/beer";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

router.use("/current", currentRouter);
router.use("/increment", incrementRouter);
router.use("/decrement", decrementRouter);
router.use("/existing", existingRouter);

// Fetch all items in database if no beerId(s) are provided
router.get("/", beer.getBeer);

// Add a new beer to database
router.post("/", beer.addNew, ok);

// Update existing beer in database (cannot update current quantity or _id)
// TODO: Only allow name updates
router.patch("/:beerId", beer.updateById, ok);

// Delete a beer from database (should not be used often)
router.delete("/:beerId", beer.deleteById, ok);

export { router };

import express from "express";
import * as beer from "../../controllers/beer";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// Check if any beer matches given information in body. Return document(s) if any existing
router.get("/", beer.isExisting);

export { router };

import express, { Request, Response, NextFunction } from "express";
import * as beer from "../../controllers/beer";
import * as breweries from "../../controllers/breweries";
import * as styles from "../../controllers/styles";

import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// PATCH beer at given id is incremented by 1, unless `amount` is provided in query
router.patch("/:beerId", beer.increment, breweries.incrementByBeerId, styles.incrementByBeerId, ok);

export { router };

import express from "express";
import * as beer from "../../controllers/beer";
import { ok } from "../../controllers/common";

const router = express.Router({ mergeParams: true });

// PATCH beer at given id is decremented by 1, unless `amount1 is provided in query
router.patch("/:beerId", beer.decrement, ok);

export { router };

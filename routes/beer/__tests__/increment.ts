import supertest from "supertest";
import * as beer from "../../../controllers/beer";

jest.spyOn(beer, "increment");

const { app } = require("../../../app");

describe("/beer/increment/:beerId", () => {
  describe("PATCH", () => {
    beforeAll(() => {
      return supertest(app).patch("/beer/increment/someId").end();
    });
    it("should call increment", () => {
      expect(beer.increment).toHaveBeenCalled();
    });
  });
});

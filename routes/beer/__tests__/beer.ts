import supertest from "supertest";
import { IBeerDocument } from "../../../models/beer";
import * as beer from "../../../controllers/beer";

jest.spyOn(beer, "getBeer");
jest.spyOn(beer, "addNew");
jest.spyOn(beer, "updateById");
jest.spyOn(beer, "deleteById");

import { app } from "../../../app";

describe("/beer", () => {
  let response: supertest.Response;

  describe("GET", () => {
    describe("without an ID", () => {
      beforeAll(() => {
        return supertest(app).get("/beer");
      });
      it("should call getBeer", () => {
        expect(beer.getBeer).toHaveBeenCalled();
      });
    });

    describe("with ID(s)", () => {
      describe("Single ID", () => {
        const beerId = "someId";
        describe("If the ID exists", () => {
          beforeAll(() => {
            return supertest(app)
              .get(`/beer?beerId=${beerId}`)
              .then((res) => (response = res));
          });
          it("should call getBeer", () => {
            expect(beer.getBeer).toHaveBeenCalled();
          });
        });
      });
    });
  });

  describe("POST", () => {
    describe("Successful request", () => {
      const newBeerDoc: IBeerDocument = {
        _id: "newId",
        name: "newBeerName",
        brewery: "newBreweryName",
        style: "newStyleName",
        container: "can/bottle",
        currentQuantity: 6,
        historicQuantity: 6,
      };

      beforeAll(() => {
        return supertest(app)
          .post("/beer")
          .send(newBeerDoc)
          .then((res) => (response = res));
      });

      it("should call addNew", () => {
        expect(beer.addNew).toHaveBeenCalled();
      });
    });
  });

  describe("PATCH", () => {
    const newBeerDoc: Partial<IBeerDocument> = {
      _id: "newId",
      name: "newBeerName",
      brewery: "newBreweryName",
      style: "newStyleName",
      container: "growler",
      currentQuantity: 1,
      historicQuantity: 1,
    };
    describe("Successfull request", () => {
      beforeAll(() => {
        return supertest(app)
          .patch("/beer/someId")
          .send(newBeerDoc)
          .then((res) => (response = res));
      });
      it("should call updateById", () => {
        expect(beer.updateById).toHaveBeenCalled();
      });
    });
  });

  describe("DELETE", () => {
    beforeAll(() => {
      return supertest(app)
        .delete("/beer/someId")
        .then((res) => (response = res));
    });
    it("should deleteById", () => {
      expect(beer.deleteById).toHaveBeenCalled();
    });
  });
});

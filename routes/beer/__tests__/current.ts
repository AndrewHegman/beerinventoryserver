import supertest from "supertest";
import * as beer from "../../../controllers/beer";

jest.spyOn(beer, "current");

const { app } = require("../../../app");

describe("/beer/current", () => {
  describe("GET", () => {
    beforeAll(() => {
      return supertest(app).get("/beer/current").end();
    });
    it("should call getBeer", () => {
      expect(beer.current).toHaveBeenCalled();
    });
  });
});

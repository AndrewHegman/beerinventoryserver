import supertest from "supertest";
import * as beer from "../../../controllers/beer";

jest.spyOn(beer, "decrement");

const { app } = require("../../../app");

describe("/beer/decrement/:beerId", () => {
  describe("PATCH", () => {
    beforeAll(() => {
      return supertest(app).patch("/beer/decrement/someId").end();
    });
    it("should call decrement", () => {
      expect(beer.decrement).toHaveBeenCalled();
    });
  });
});

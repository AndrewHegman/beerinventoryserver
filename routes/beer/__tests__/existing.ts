import supertest from "supertest";
import * as beer from "../../../controllers/beer";

jest.spyOn(beer, "isExisting");

const { app } = require("../../../app");

describe("/beer/existing", () => {
  describe("GET", () => {
    beforeAll(() => {
      return supertest(app).get("/beer/existing").end();
    });
    it("should call isExisting", () => {
      expect(beer.isExisting).toHaveBeenCalled();
    });
  });
});

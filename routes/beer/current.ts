import express from "express";
import * as beer from "../../controllers/beer";

const router = express.Router({ mergeParams: true });

// GET all current beer
router.get("/", beer.current);

export { router };

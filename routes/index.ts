import express from "express";

const router = express.Router({ mergeParams: true });

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

export { router };

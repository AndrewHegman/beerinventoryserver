import express, { NextFunction } from "express";
import path from "path";
import createError from "http-errors";
import { router as indexRouter } from "./routes";
import { router as beerRouter } from "./routes/beer/beer";
import { router as breweryRouter } from "./routes/breweries/breweries";
import { router as stylesRouter } from "./routes/styles/styles";
import { version } from "./Utils/version";
import cors from "cors";

const logger = require("morgan");
const cookieParser = require("cookie-parser");

const app = express();

app.set("query parser", "simple");
// view engine setup
// app.set("views", path.join(__dirname, "views"));
// app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

app.use(`/${version}/`, indexRouter);
app.use(`/${version}/beer`, beerRouter);
app.use(`/${version}/breweries`, breweryRouter);
app.use(`/${version}/styles`, stylesRouter);

// catch 404 and forward to error handler
app.use(function (req: any, res: any, next: NextFunction) {
  next(createError(404));
});

// error handler
app.use(function (err: any, req: any, res: any, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render("error");
});

export { app };
module.exports = app;

const { series, parallel, src, dest } = require("gulp");
const clean = require("gulp-clean");
const babel = require("gulp-babel");
const typescript = require("gulp-tsc");

function _clean() {
  return src(["bin/*", "!bin/www"]).pipe(clean());
}

function transpileRoutes() {
  return src("routes/**/*.ts").pipe(typescript()).pipe(dest("bin/routes"));
}

function transpilePublic() {
  return src("public/**/*.ts").pipe(typescript()).pipe(dest("bin/public"));
}

function transpileApp() {
  return src("app.ts").pipe(typescript()).pipe(dest("bin"));
}

function transpileDb() {
  return src("db.ts").pipe(typescript()).pipe(dest("bin"));
}

function transpileControllers() {
  return src("controllers/*.ts").pipe(typescript()).pipe(dest("bin/controllers"));
}

function transpileModels() {
  return src("models/*.ts").pipe(typescript()).pipe(dest("bin/models"));
}

function copyPublic() {
  return src(["public/**", "!public/**/*.ts"]).pipe(dest("bin/public"));
}

function copyViews() {
  return src("views/*").pipe(dest("bin/views"));
}

exports.clean = _clean;
exports.transpile = parallel(
  transpileRoutes,
  transpilePublic,
  transpileApp,
  transpileControllers,
  transpileModels
);
exports.copy = parallel(copyPublic, copyViews);
exports.build = parallel(
  transpileRoutes,
  transpilePublic,
  transpileApp,
  transpileControllers,
  transpileDb,
  transpileModels,
  copyPublic,
  copyViews
);
exports.cleanAndBuild = series(
  _clean,
  parallel(
    transpileRoutes,
    transpilePublic,
    transpileApp,
    transpileControllers,
    transpileModels,
    copyPublic,
    copyViews
  )
);

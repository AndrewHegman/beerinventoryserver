import { MongoError, MongoClient, Db } from "mongodb";
import { getDatabaseName } from "./Utils/database-names";
require("dotenv");

type MongoErrorCallback = (err?: MongoError) => void;

const state: { client: MongoClient; db: Db } = {
  client: null,
  db: null,
};

export const connect = (url: string, done: MongoErrorCallback) => {
  if (state.db) return done();

  MongoClient.connect(url, (err, db) => {
    if (err) return done(err);
    state.client = db;
    state.db = db.db(getDatabaseName());
    done();
  });
};

export const get = (): Db => {
  return state.db;
};

export const close = (done: MongoErrorCallback): void => {
  if (state.db) {
    state.client.close(function (err, result) {
      state.db = null;
      done(err);
    });
  }
};

export const getBeerCollection = () => get().collection("Beer");
export const getBreweriesCollection = () => get().collection("Breweries");
export const getStylesCollection = () => get().collection("Styles");

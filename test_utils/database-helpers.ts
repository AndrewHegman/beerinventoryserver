import { Collection } from "mongodb";
import { IBeerDocument } from "../models/beer";
import { IBreweryDocument } from "../models/breweries";

export const dropcollection = (collection: Collection) => {
  return collection.deleteMany({});
};

export const populateCollection = (
  collection: Collection,
  documents: IBeerDocument[] | IBreweryDocument[]
) => {
  return collection.insertMany(documents);
};

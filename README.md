## Beer Inventory Server

This is the repository of the server for the (BeerInventory)[https://gitlab.com/AndrewHegman/BeerInventory] app.

## `Starting the server for development`

To start the server for development, run `npm run startWatch`. This will run a watcher to watch for code changes and will automatically re-transpile.

## `Starting the server for production`

To host the server for proudction, run `npm start`. This will _not_ initially compile/transpile the code. This should be done prior to hosting the server.

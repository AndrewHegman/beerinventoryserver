import { Request, Response, NextFunction } from "express";
import { getBeerCollection } from "../db";
import { buildExpandFieldQuery, Collections } from "./common";

// export const getBeer = (req: Request, res: Response, next: NextFunction) => {
//   const { expand, ...queryParams } = req.query;

//   if (req.query?.beerId) {
//     return byId(req, res, next);
//   } else if (Object.keys(req.query).length > 0 && !req.query.expand) {
//     return byMetadata(req, res, next);
//   }
//   return all(req, res, next);
// };

export const getBeer = (req: Request, res: Response, next: NextFunction) => {
  const beerIds = req.query["_id"] ? (req.query["_id"] as string).split(",") : [];

  const expandFieldQuery = buildExpandFieldQuery(Collections.Beer, req.query.expand as string);
  const aggregatePredicate = beerIds.length > 0 ? [{ $match: { $or: beerIds.map((beerId) => ({ _id: beerId })) } }] : [];

  if (expandFieldQuery) {
    aggregatePredicate.push(...expandFieldQuery);
  }

  return getBeerCollection()
    .aggregate(aggregatePredicate)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      return res.send(docs);
    });
};

// const all = (req: Request, res: Response, next: NextFunction) => {
//   const expandFieldQuery = buildExpandFieldQuery(Collections.Beer, req.query.expand as string);
//   const aggregatePredicate = [];

//   if (expandFieldQuery) {
//     aggregatePredicate.push(...expandFieldQuery);
//   }

//   console.log(aggregatePredicate);
//   return getBeerCollection()
//     .aggregate(aggregatePredicate)
//     .toArray((err, docs) => {
//       if (err) {
//         return res.status(400).send(err);
//       }
//       return res.send(docs);
//     });
// };

// const byId = (req: Request, res: Response, next: NextFunction) => {
//   const beerIds = (req.query.beerId as string).split(",");
//   const expandFieldQuery = buildExpandFieldQuery(Collections.Beer, req.query.expand as string);

//   const aggregatePredicate = [{ $match: { $or: beerIds.map((beerId) => ({ _id: beerId })) } }];
//   if (expandFieldQuery) {
//     aggregatePredicate.push(...expandFieldQuery);
//   }

//   return getBeerCollection()
//     .aggregate(aggregatePredicate)
//     .toArray((err, docs) => {
//       if (err) {
//         return res.status(400).send(err);
//       }
//       return res.send(docs);
//     });
// };

// const byMetadata = (req: Request, res: Response, next: NextFunction) => {
//   console.log(Object.keys(req.query));
//   const aggregatePredicate = Object.keys(req.query).map((key) => {
//     return { $match: { [key]: req.query[key] } };
//   });
//   getBeerCollection()
//     .aggregate(aggregatePredicate)
//     .toArray((err, docs) => {
//       if (err) {
//         return res.status(400).send(err);
//       }
//       return res.send(docs);
//     });
// };

export const current = (req: Request, res: Response, next: NextFunction) => {
  const expandFieldQuery = buildExpandFieldQuery(Collections.Beer, req.query.expand as string);
  const aggregatePredicate = [{ $match: { currentQuantity: { $gt: 0 } } }];

  if (expandFieldQuery) aggregatePredicate.push(...expandFieldQuery);
  getBeerCollection()
    .aggregate(aggregatePredicate)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(docs);
    });
};

export const addNew = (req: Request, res: Response, next: NextFunction) => {
  if (req.body.quantity) {
    req.body.historicQuantity = req.body.quantity;
    req.body.currentQuantity = req.body.quantity;
  }

  getBeerCollection().insertOne(req.body, (result) => {
    if (result) {
      return res.status(400).send(result);
    }
    next();
  });
};

export const isExisting = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection()
    .find(req.body)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(docs);
    });
};

export const updateById = (req: Request, res: Response, next: NextFunction) => {
  // Make sure _id isn't updated. Will throw MongoError if attempted.
  delete req.body._id;

  // We don't allow quantity(ies) to be updated here
  delete req.body.currentQuantity;
  delete req.body.historicQuantity;

  getBeerCollection().findOneAndUpdate({ _id: req.params.beerId }, { $set: { ...req.body } }, (err, docs) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (!docs.lastErrorObject.updatedExisting) {
      return res.sendStatus(404);
    }
    next();
  });
};

export const deleteById = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection().findOneAndDelete({ _id: req.params.beerId }, (result) => {
    res.send(result);
    next();
  });
};

export const increment = (req: Request, res: Response, next: NextFunction) => {
  const incrementAmount = req.query.amount ? Number(req.query.amount) : 1;
  getBeerCollection().findOneAndUpdate(
    { _id: req.params.beerId },
    { $inc: { currentQuantity: incrementAmount, historicQuantity: incrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        console.log(req.params.beerId);
        return res.sendStatus(404);
      }

      next();
    }
  );
};

export const decrement = (req: Request, res: Response, next: NextFunction) => {
  const decrementAmount = req.query.amount ? Number(req.query.amount) : 1;

  getBeerCollection().findOneAndUpdate(
    { _id: req.params.beerId },
    { $inc: { currentQuantity: -decrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        return res.sendStatus(404);
      }

      next();
    }
  );
};

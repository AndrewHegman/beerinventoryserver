import { Request, Response, NextFunction } from "express";
import { expandableFieldsMap as beerEFM, expandableFields as beerEF } from "../models/beer";
import { expandableFieldsMap as breweriesEFM, expandableFields as breweriesEF } from "../models/breweries";
import { expandableFieldsMap as stylesEFM, expandableFields as stylesEF } from "../models/styles";

export const enum Collections {
  Beer = "beer",
  Breweries = "breweries",
  Styles = "styles",
  Geography = "geography",
}

const getExpandableFields = (collection: Collections) => {
  switch (collection) {
    case Collections.Beer:
      return beerEF;
    case Collections.Breweries:
      return breweriesEF;
    case Collections.Styles:
      return stylesEF;
    default:
      return [];
  }
};

const getExpandableFieldsMap = (collection: Collections) => {
  switch (collection) {
    case Collections.Beer:
      return beerEFM;
    case Collections.Breweries:
      return breweriesEFM;
    case Collections.Styles:
      return stylesEFM;
    default:
      return [];
  }
};

export const ok = (req: Request, res: Response, next: NextFunction) => {
  return res.sendStatus(200);
};

export const buildExpandFieldQuery = (collection: Collections, queryString: string): any[] => {
  const expandableFields = getExpandableFields(collection);
  const expandableFieldsMap = getExpandableFieldsMap(collection);

  if (!queryString) return [];
  else {
    const predicate = queryString
      ?.split(",")
      .filter((field) => expandableFields.indexOf(field) > -1)
      .map((validField) => [{ $lookup: expandableFieldsMap[validField] }, { $unwind: `$${validField}` }]);
    return predicate[0].concat(...predicate.splice(1));
  }
};

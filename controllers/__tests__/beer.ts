import httpMocks from "node-mocks-http";
import * as beer from "../beer";
import { getBeerCollection } from "../../db";

jest.mock("../../db");

describe("Beer Controller", () => {
  const mockNext = jest.fn();
  describe("getBeer", () => {
    beforeEach(() => {
      const req = httpMocks.createRequest();
      const res = httpMocks.createResponse();

      beer.getBeer(req, res, mockNext);
    });
    it("Fetches all entries from database", () => {});
  });
});

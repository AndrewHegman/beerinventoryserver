import { Request, Response, NextFunction } from "express";
import { getBeerCollection, getBreweriesCollection } from "../db";
import { IBeerDocument } from "../models/beer";
import { buildExpandFieldQuery, Collections } from "./common";

export const getBreweries = (req: Request, res: Response, next: NextFunction) => {
  if (req.query.beerId) {
    return byId(req, res, next);
  }
  return all(req, res, next);
};

const all = (req: Request, res: Response, next: NextFunction) => {
  const expandFieldQuery = buildExpandFieldQuery(Collections.Breweries, req.query.expand as string);
  const aggregatePredicate = [];

  if (expandFieldQuery) aggregatePredicate.push(...expandFieldQuery);
  console.log(aggregatePredicate);
  return getBreweriesCollection()
    .aggregate(aggregatePredicate)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      return res.send(docs);
    });
};

const byId = (req: Request, res: Response, next: NextFunction) => {
  return _byId((req.query.beerId as string).split(","), req.query.expandFields as string, res, next);
};

const _byId = (breweryIds: string[], expandFields: string, res: Response, next: NextFunction) => {
  const expandFieldQuery = buildExpandFieldQuery(Collections.Breweries, expandFields);

  const aggregatePredicate = [{ $match: { $or: breweryIds.map((beerId) => ({ _id: beerId })) } }];
  if (expandFieldQuery) aggregatePredicate.push(...expandFieldQuery);

  return getBreweriesCollection()
    .aggregate(aggregatePredicate)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(docs);
    });
};

export const current = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection()
    .find({ currentQuantity: { $gt: 0 } })
    .toArray((err, docs: IBeerDocument[]) => {
      if (err) {
        return res.send(err);
      }
      _byId(
        docs.map((doc) => doc.brewery),
        req.query.expand as string,
        res,
        next
      );
    });
};

export const addNew = (req: Request, res: Response, next: NextFunction) => {
  getBreweriesCollection().insertOne(req.body, (result) => {
    if (result) {
      return res.status(400).send(result);
    }

    next();
  });
};

export const isExisting = (req: Request, res: Response, next: NextFunction) => {
  getBreweriesCollection()
    .find(req.body)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(docs);
      next();
    });
};

export const updateById = (req: Request, res: Response, next: NextFunction) => {
  // Make sure _id isn't updated. Will throw MongoError if attempted.
  if (req.body._id) {
    delete req.body._id;
  }

  getBreweriesCollection().findOneAndUpdate({ _id: req.params.breweryId }, { $set: { ...req.body } }, (err, docs) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (docs.lastErrorObject.n < 1) {
      return res.status(404).send(docs);
    }

    next();
  });
};

export const deleteById = (req: Request, res: Response, next: NextFunction) => {
  getBreweriesCollection().findOneAndDelete({ _id: req.params.breweryId }, (err, result) => {
    if (result.lastErrorObject.n < 1) {
      res.status(404).send(result);
    }

    next();
  });
};

export const incrementByBeerId = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection().findOne({ _id: req.params.beerId }, (err, doc) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (!doc) {
      return res.sendStatus(404);
    }
    increment(doc.brewery, req, res, next);
  });
};

export const incrementByBreweryId = (req: Request, res: Response, next: NextFunction) => {
  increment(req.params.breweryId, req, res, next);
};

const increment = (breweryId: string, req: Request, res: Response, next: NextFunction) => {
  const incrementAmount = req.query.amount ? Number(req.query.amount) : 1;
  getBreweriesCollection().findOneAndUpdate({ _id: breweryId }, { $inc: { count: incrementAmount } }, { returnOriginal: false }, (err, docs) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (!docs.lastErrorObject.updatedExisting) {
      return res.sendStatus(404);
    }

    next();
  });
};

export const decrementByBreweryId = (req: Request, res: Response, next: NextFunction) => {
  const decrementAmount = req.query.amount ? Number(req.query.amount) : 1;

  getBreweriesCollection().findOneAndUpdate(
    { _id: req.params.breweryId },
    { $inc: { count: -decrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        return res.sendStatus(404);
      }

      next();
    }
  );
};

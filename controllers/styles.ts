import { Request, Response, NextFunction } from "express";
import { getStylesCollection, getBeerCollection } from "../db";

export const getStyles = (req: Request, res: Response, next: NextFunction) => {
  if ((req.query.styleId as string)?.split(",").length > 0) {
    return byId(req, res, next);
  }
  return all(req, res, next);
};

const all = (req: Request, res: Response, next: NextFunction) => {
  getStylesCollection()
    .find({})
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }

      res.send(docs);
    });
};

const byId = (req: Request, res: Response, next: NextFunction) => {
  const styleIds = (req.query.styleId as string).split(",");
  getStylesCollection()
    .find({ $or: styleIds.map((styleId) => ({ _id: styleId })) })
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }

      if (!docs) {
        return res.sendStatus(404);
      }

      res.send(docs);
    });
};

export const current = (req: Request, res: Response, next: NextFunction) => {
  getStylesCollection()
    .find({ "substyles.count": { $gt: 0 } })
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }
      res.send(docs);
    });
};

export const getSubstyles = (req: Request, res: Response, next: NextFunction) => {
  getStylesCollection()
    .find({ isBaseStyle: false })
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }

      if (docs?.length < 1) {
        return res.sendStatus(404);
      }

      res.send(docs);
    });
};

export const getBaseStyles = (req: Request, res: Response, next: NextFunction) => {
  console.log("base");
  getStylesCollection()
    .find({ isBaseStyle: true })
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }

      if (docs?.length < 1) {
        return res.sendStatus(404);
      }

      res.send(docs);
    });
};

export const addNew = (req: Request, res: Response, next: NextFunction) => {
  getStylesCollection().insertOne(req.body, (result) => {
    if (result) {
      return res.status(400).send(result);
    }
    next();
  });
};

export const incrementByBeerId = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection().findOne({ _id: req.params.beerId }, (err, doc) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (!doc) {
      return res.sendStatus(404);
    }
    increment(doc.style, req, res, next);
  });
};

const increment = (styleId: string, req: Request, res: Response, next: NextFunction) => {
  const incrementAmount = req.query.amount ? Number(req.query.amount) : 1;
  getStylesCollection().findOneAndUpdate(
    { _id: styleId },
    { $inc: { count: incrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        return res.sendStatus(404);
      }

      next();
    }
  );
};

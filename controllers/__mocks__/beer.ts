import { Request, Response, NextFunction } from "express";
import { mockBeer } from "../../test_utils/mock-dataset";
import { getBeerCollection } from "../../db";
import { IBeerDocument } from "../../models/beer";

export const getBeer = (req: Request, res: Response, next: NextFunction) => {
  if (req.query.beerId?.split(",").length > 0) {
    return byId(req, res, next);
  }
  return all(req, res, next);
};

const all = (req: Request, res: Response, next: NextFunction) => {
  return res.send(mockBeer);
};

const byId = (req: Request, res: Response, next: NextFunction) => {
  const beerIds: string[] = req.query.beerId.split(",");
  return res.send(mockBeer.filter((beer) => beerIds.indexOf(beer._id) >= 0));
};

export const current = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection()
    .find({ currentQuantity: { $gt: 0 } })
    .toArray((err, docs) => {
      if (err) {
        return res.send(err);
      }
      res.send(docs);
    });
};

export const addNew = (req: Request, res: Response, next: NextFunction) => {
  if (req.body.quantity) {
    req.body.historicQuantity = req.body.quantity;
    req.body.currentQuantity = req.body.quantity;
  }

  getBeerCollection().insertOne(req.body, (result) => {
    if (result) {
      return res.status(400).send(result);
    }
    next();
  });
};

export const isExisting = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection()
    .find(req.body)
    .toArray((err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(docs);
    });
};

export const updateById = (req: Request, res: Response, next: NextFunction) => {
  // Make sure _id isn't updated. Will throw MongoError if attempted.
  delete req.body._id;

  // We don't allow quantity(ies) to be updated here
  delete req.body.currentQuantity;
  delete req.body.historicQuantity;

  getBeerCollection().findOneAndUpdate(
    { _id: req.params.beerId },
    { $set: { ...req.body } },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }
      next();
    }
  );
};

export const deleteById = (req: Request, res: Response, next: NextFunction) => {
  getBeerCollection().findOneAndDelete({ _id: req.params.beerId }, (result) => {
    res.send(result);
    next();
  });
};

export const increment = (req: Request, res: Response, next: NextFunction) => {
  const incrementAmount = req.query.amount ? Number(req.query.amount) : 1;
  getBeerCollection().findOneAndUpdate(
    { _id: req.params.beerId },
    { $inc: { currentQuantity: incrementAmount, historicQuantity: incrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        return res.sendStatus(404);
      }

      next();
    }
  );
};

export const decrement = (req: Request, res: Response, next: NextFunction) => {
  const decrementAmount = req.query.amount ? Number(req.query.amount) : 1;

  getBeerCollection().findOneAndUpdate(
    { _id: req.params.beerId },
    { $inc: { currentQuantity: -decrementAmount } },
    { returnOriginal: false },
    (err, docs) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!docs.lastErrorObject.updatedExisting) {
        return res.sendStatus(404);
      }

      next();
    }
  );
};
